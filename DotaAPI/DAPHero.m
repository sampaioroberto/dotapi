//
//  DAPHero.m
//  DotaAPI
//
//  Created by Roberto Sampaio on 04/11/15.
//  Copyright © 2015 Roberto Sampaio. All rights reserved.
//

#import "DAPHero.h"

@interface DAPHero()

@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) NSInteger idNumber;
@property (nonatomic, strong) NSString *urlPicture;

@end

@implementation DAPHero

-(instancetype)initWithDictionary:(NSDictionary *)dictionary{
    if (self = [super init]) {
        NSString *localizedName = [self formattedNameWithName: dictionary[@"localized_name"]];
        self.name = localizedName;
        self.idNumber = (NSInteger) dictionary[@"id"];
        
        self.urlPicture = [NSString stringWithFormat: @"http://cdn.dota2.com/apps/dota2/images/heroes/%@.png", [self formattedUrlPictureWithName: dictionary[@"name"]]];

    }
    return self;
}
                          
- (NSString *) formattedNameWithName:(NSString *)name{
    name = [name stringByReplacingOccurrencesOfString:@"npc_dota_hero_" withString:@""];
    name = [name stringByReplacingOccurrencesOfString:@"_" withString:@" "];
    name = [name capitalizedString];
    return name;
}

- (NSString *) formattedUrlPictureWithName:(NSString *)name{
    name = [name stringByReplacingOccurrencesOfString:@"npc_dota_hero_" withString:@""];
    name = [NSString stringWithFormat:@"%@_full", name];
    return name;
}
@end
