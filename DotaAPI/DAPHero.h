//
//  DAPHero.h
//  DotaAPI
//
//  Created by Roberto Sampaio on 04/11/15.
//  Copyright © 2015 Roberto Sampaio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DAPHero : NSObject

@property (nonatomic, strong, readonly) NSString *name;
@property (nonatomic, assign, readonly) NSInteger idNumber;
@property (nonatomic, strong, readonly) NSString *urlPicture;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
