//
//  StringUtils.swift
//  Carros
//
//  Created by Ricardo Lecheta on 7/1/14.
//  Copyright (c) 2014 Ricardo Lecheta. All rights reserved.
//

import Foundation

class StringUtils {
    
    class func toString(data: NSData!) -> NSString! {
        if(data == nil) {
            return nil
        }
        let s = NSString(data: data, encoding: NSUTF8StringEncoding)
        return s
    }
    
    class func toNSData(s: String) -> NSData {
        let data = s.dataUsingEncoding(NSUTF8StringEncoding)
        return data!
    }
    
    class func toCString(s: String) -> UnsafePointer<Int8> {
        // cast to NSString
        // const char *
        let cstring = ("string" as NSString).UTF8String
        return cstring
    }
    
    class func trim(s: NSString) -> NSString {
        return s.stringByTrimmingCharactersInSet(.whitespaceAndNewlineCharacterSet())
    }
    
    class func replace(s: String, string: String, withString: String) -> String {
        return s.stringByReplacingOccurrencesOfString(string, withString: withString, options: NSStringCompareOptions.LiteralSearch, range: nil)
    }
}