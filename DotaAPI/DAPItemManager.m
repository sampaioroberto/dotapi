//
//  DAPItemManager.m
//  DotaAPI
//
//  Created by Roberto Sampaio on 16/02/16.
//  Copyright © 2016 Roberto Sampaio. All rights reserved.
//

#import "DAPItemManager.h"
#import "DAPItem.h"

@implementation DAPItemManager

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"items": @"result.items"
             };
}

+ (NSValueTransformer *)itemsJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^(NSArray *values, BOOL *success, NSError **error){
        NSMutableArray *items = [NSMutableArray array];
        for (NSDictionary *itemDic in values){
            DAPItem *item = [[DAPItem alloc] initWithDictionary:itemDic];
            [items addObject:item];
        }
        return [NSArray arrayWithArray:items];
    } reverseBlock:^(NSArray *heroesArray, BOOL *success, NSError **error) {
        NSMutableArray *values = [NSMutableArray array];
        for (DAPItem *item in heroesArray){
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            dic[@"name"] = item.name;
            dic[@"id"] = [NSNumber numberWithInteger:item.idNumber];
            [values addObject:item];
        }
        return [NSArray arrayWithArray:values];
    }];
}


@end
