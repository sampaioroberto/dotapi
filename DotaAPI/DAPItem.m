//
//  DAPItem.m
//  DotaAPI
//
//  Created by Roberto Sampaio on 16/02/16.
//  Copyright © 2016 Roberto Sampaio. All rights reserved.
//

#import "DAPItem.h"

@interface DAPItem()

@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) NSInteger idNumber;
@property (nonatomic, strong) NSString *urlPicture;

@end

@implementation DAPItem

-(instancetype)initWithDictionary:(NSDictionary *)dictionary{
    if (self = [super init]) {
        NSString *localizedName = [self formattedNameWithName: dictionary[@"localized_name"]];
        self.name = localizedName;
        self.idNumber = (NSInteger) dictionary[@"id"];
        
        self.urlPicture = [NSString stringWithFormat: @"http://cdn.dota2.com/apps/dota2/images/items/%@.png", [self formattedUrlPictureWithName: dictionary[@"name"]]];
    }
    return self;
}

- (NSString *) formattedNameWithName:(NSString *)name{
    name = [name stringByReplacingOccurrencesOfString:@"item_" withString:@""];
    name = [name stringByReplacingOccurrencesOfString:@"_" withString:@" "];
    name = [name capitalizedString];
    return name;
}

- (NSString *) formattedUrlPictureWithName:(NSString *)name{
    name = [name stringByReplacingOccurrencesOfString:@"item_" withString:@""];
    name = [NSString stringWithFormat:@"%@_lg", name];
    return name;
}

@end
