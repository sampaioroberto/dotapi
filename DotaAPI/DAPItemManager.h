//
//  DAPItemManager.h
//  DotaAPI
//
//  Created by Roberto Sampaio on 16/02/16.
//  Copyright © 2016 Roberto Sampaio. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface DAPItemManager : MTLModel<MTLJSONSerializing>

@property (nonatomic, strong) NSArray *items;

@end
