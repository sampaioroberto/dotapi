//
//  DotaAPI.m
//  DotaAPI
//
//  Created by Roberto Sampaio on 04/11/15.
//  Copyright © 2015 Roberto Sampaio. All rights reserved.
//

#import "DAPDotaAPI.h"
#import "DAPHeroManager.h"
#import "Mantle.h"
#import "DAPDotaConstants.h"
#import "DAPHero.h"
#import "DAPItemManager.h"
#import "DAPItem.h"

@import UIKit;

@implementation DAPDotaAPI

- (void *) heroesWithCompletion:(void (^)(NSArray *heroes))completion{
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [queue addOperationWithBlock:^{
        NSURL *url = [NSURL URLWithString:@"https://api.steampowered.com/IEconDOTA2_570/GetHeroes/v0001/?key=055DB6245676C2E32DDBBA794AF71932&language=pt-br"];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithURL:url completionHandler: ^(NSData *data, NSURLResponse *response, NSError *error){
            NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
            
            DAPHeroManager *heroManager = [MTLJSONAdapter modelOfClass:DAPHeroManager.class fromJSONDictionary:dic error:NULL];
            NSArray *heroes = heroManager.heroes;
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                completion(heroes);
            }];
            
        }];
        [task resume];
    }];

    
    return nil;
}

- (void *) itemsWithCompletion:(void (^)(NSArray *items))completion{
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [queue addOperationWithBlock:^{
        NSURL *url = [NSURL URLWithString:@"https://api.steampowered.com/IEconDOTA2_570/GetGameItems/V001/?key=055DB6245676C2E32DDBBA794AF71932&language=pt-br"];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithURL:url completionHandler: ^(NSData *data, NSURLResponse *response, NSError *error){
            NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
            
            DAPItemManager *itemManager = [MTLJSONAdapter modelOfClass:DAPItemManager.class fromJSONDictionary:dic error:NULL];
            NSArray *items = itemManager.items;
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                completion(items);
            }];
            
        }];
        [task resume];
    }];
    
    
    return nil;
}


- (UIImage *)imageWithHero:(DAPHero *)hero{
    NSString *name = hero.name;
    name = [name stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    return nil;
    
}

@end
