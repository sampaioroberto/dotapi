//
//  DotaAPI.h
//  DotaAPI
//
//  Created by Roberto Sampaio on 04/11/15.
//  Copyright © 2015 Roberto Sampaio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DAPDotaAPI : NSObject

- (void *) heroesWithCompletion:(void (^)(NSArray *heroes))completion;
- (void *) itemsWithCompletion:(void (^)(NSArray *items))completion;

@end
