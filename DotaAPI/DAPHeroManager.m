
//
//  DAPHeroManager.m
//  DotaAPI
//
//  Created by Roberto Sampaio on 12/12/15.
//  Copyright © 2015 Roberto Sampaio. All rights reserved.
//

#import "DAPHeroManager.h"
#import "DAPHero.h"

@implementation DAPHeroManager

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"count": @"result.count",
             @"heroes": @"result.heroes"
             };
}

+ (NSValueTransformer *)heroesJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^(NSArray *values, BOOL *success, NSError **error){
            NSMutableArray *heroes = [NSMutableArray array];
        for (NSDictionary *heroDic in values){
            DAPHero *hero = [[DAPHero alloc] initWithDictionary:heroDic];
            [heroes addObject:hero];
        }
        return [NSArray arrayWithArray:heroes];
    } reverseBlock:^(NSArray *heroesArray, BOOL *success, NSError **error) {
        NSMutableArray *values = [NSMutableArray array];
        for (DAPHero *hero in heroesArray){
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            dic[@"name"] = hero.name;
            dic[@"id"] = [NSNumber numberWithInteger:hero.idNumber];
            [values addObject:hero];
        }
        return [NSArray arrayWithArray:values];
    }];
}

@end
