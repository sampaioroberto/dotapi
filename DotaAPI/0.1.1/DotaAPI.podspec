Pod::Spec.new do |s|

# 1
s.platform = :ios
s.ios.deployment_target = '8.0'
s.name = "DotaAPI"
s.summary = "DotaAPI lets a user select an ice cream flavor."
s.requires_arc = true

# 2
s.version = "0.1.1"

# 3
s.license = { :type => "MIT", :file => "LICENSE" }

# 4 - Replace with your name and e-mail address
s.author = { "Roberto Sampaio" => "ssampaio.roberto@gmail.com" }

# 5 - Replace this URL with your own Github page's URL (from the address bar)
s.homepage = "http://www.bitbucketcards.com/sampaioroberto/dotapi"

# 6 - Replace this URL with your own Git URL from "Quick Setup"
s.source = { :git => "https://sampaioroberto@bitbucket.org/sampaioroberto/dotapi.git", :tag => "#{s.version}"}

# 7
s.framework = "UIKit"
s.dependency 'Mantle'

# 8
s.source_files = "DotaAPI/**/*.{h,m}"

end