//
//  DAPItem.h
//  DotaAPI
//
//  Created by Roberto Sampaio on 16/02/16.
//  Copyright © 2016 Roberto Sampaio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DAPItem : NSObject

@property (nonatomic, strong, readonly) NSString *name;
@property (nonatomic, assign, readonly) NSInteger idNumber;
@property (nonatomic, strong, readonly) NSString *urlPicture;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
