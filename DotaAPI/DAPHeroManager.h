//
//  DAPHeroManager.h
//  DotaAPI
//
//  Created by Roberto Sampaio on 12/12/15.
//  Copyright © 2015 Roberto Sampaio. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface DAPHeroManager : MTLModel <MTLJSONSerializing>

@property (nonatomic, assign) NSInteger count;
@property (nonatomic, strong) NSArray *heroes;

@end
